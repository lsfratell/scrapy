import asyncio
import json
import js2py
import datetime
from pony.orm.core import TransactionIntegrityError, UnexpectedError
from models import *
from aiohttp import ClientSession, ClientConnectionError
from time import time, gmtime, strftime
from slugify import slugify
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor

class Scrapy:

    css = {
        'name': '#main > div.d14 > div > div.d37 > div.d39 > table > tbody > tr:nth-child(1) > td:nth-child(2) > span',
        'alternateNames': '#main > div.d14 > div > div.d37 > div.d39 > table > tbody > tr:nth-child(2) > td:nth-child(2)',
        'yearOfRelease': '#main > div.d14 > div > div.d37 > div.d39 > table > tbody > tr:nth-child(3) > td:nth-child(2)',
        'status': '#main > div.d14 > div > div.d37 > div.d39 > table > tbody > tr:nth-child(4) > td:nth-child(2)',
        'author': '#main > div.d14 > div > div.d37 > div.d39 > table > tbody > tr:nth-child(5) > td:nth-child(2)',
        'artist': '#main > div.d14 > div > div.d37 > div.d39 > table > tbody > tr:nth-child(6) > td:nth-child(2)',
        'readingDirecion': '#main > div.d14 > div > div.d37 > div.d39 > table > tbody > tr:nth-child(7) > td:nth-child(2)',
        'genres': '#main > div.d14 > div > div.d37 > div.d39 > table > tbody > tr:nth-child(8) > td:nth-child(2) a',
        'description': '#main > div.d14 > div > div.d46 > p',
        'chapters': '.d48 td a',
        'capa': '#main > div.d14 > div > div.d37 > div.d38 > img'
    }

    genres = [
        'Action','Adventure','Comedy','Demons',
        'Drama','Ecchi','Fantasy','Gender Bender',
        'Harem','Historical','Horror','Josei','Magic',
        'Martial Arts','Mature','Mecha','Military',
        'Mystery','One Shot','Psychological','Romance',
        'School Life','Sci-Fi','Seinen','Shoujo','Shoujoai',
        'Shounen','Shounenai','Slice of Life','Smut','Sports',
        'Super Power','Supernatural','Tragedy','Vampire','Yaoi','Yuri'
    ]

    def __init__(self, loop, sema, to_fetch=10):
        self.loop = loop
        self.sema = sema
        self.to_fetch = to_fetch
        self.base_url = 'https://www.mangareader.net'
        self.executor = ThreadPoolExecutor()

    @db_session
    async def start(self):
        if len(Genre.select()) == 0:
            for g in self.genres:
                Genre(name=g, slug=self.slugify(g))
            commit()
        await self.get_latest_mangas()

    @db_session
    def manga_exists(self, slug):
        m = Manga.get(slug=slug)
        if m:
            return m.get_pk()
        return 0

    @db_session
    def insert_manga(self, data):
        print('[INSERTING] Manga:', data.get('slug'))
        if data.get('slug') == '':
            return
        manga = Manga(
            name=data.get('name'),
            slug=data.get('slug'),
            capa=data.get('capa'),
            year=data.get('year'),
            status=data.get('status'),
            direction=data.get('direction'),
            description=data.get('description')
        )
        genres = []
        for genre in data.get('genres'):
            genres.append(Genre.get(slug=self.slugify(genre)))
        chapters = []
        for chapter in data.get('chapters'):
            c = Chapter(
                name=chapter.get('name'),
                number=chapter.get('number'),
                manga=manga
            )
            c.imgs = [Img(chapter=c, manga=manga, path=i) for i in chapter.get('imgs')]
            chapters.append(c)
        manga.genres = genres
        manga.chapters = chapters
        manga.flush()

    @db_session
    async def update_manga(self, data, manga_id):
        print('[UPDATING] Manga:', data.get('slug'))
        manga = Manga[manga_id]
        chapters = []
        tasks = []
        for chapter in data.get('chapters'):
            c = Chapter.get(manga=manga, number=chapter.get('number'))
            if not c:
                chapters.append(chapter)
                tasks.append(self.loop.create_task(self.get_chapter_imgs(chapter)))
        if len(chapters) > 0:
            print('[UPDATING] Getting imgs of manga:', data.get('slug'))
            await asyncio.gather(*tasks)
            for chapter in chapters:
                c = Chapter(name=chapter.get('name'), number=chapter.get('number'), manga=manga)
                c.imgs = [Img(chapter=c, manga=manga, path=i) for i in chapter.get('imgs')]
                manga.chapters.add(c)
            ch_updated_at = datetime.utcnow()
            manga.flush()

    async def get_manga_data(self, path):
        html = await self.fetch_page(path)
        html = await self.loop.run_in_executor(self.executor, BeautifulSoup, html, 'html5lib')
        data = {
            'capa': self.select_capa(html, self.css.get('capa')),
            'name': self.select_one(html, self.css.get('name')),
            'slug': self.slugify(self.select_one(html, self.css.get('name'))),
            'year': self.select_one(html, self.css.get('yearOfRelease')),
            'status': self.select_one(html, self.css.get('status')),
            'direction': self.select_one(html, self.css.get('readingDirecion')),
            'description': self.select_one(html, self.css.get('description')),
            'genres': self.select_genres(html, self.css.get('genres')),
            'chapters': self.select_chapters(html, self.css.get('chapters'))
        }
        manga_id = await self.loop.run_in_executor(self.executor, self.manga_exists, data.get('slug'))
        if not manga_id:
            chapter_tasks = []
            for chapter in data.get('chapters'):
                chapter_tasks.append(self.loop.create_task(self.get_chapter_imgs(chapter)))
            print('[INSERTING] Getting imgs of manga:', data.get('slug'))
            await asyncio.gather(*chapter_tasks)
            await self.loop.run_in_executor(self.executor, self.insert_manga, data)
        else:
            await self.update_manga(data, manga_id)

    async def get_chapter_imgs(self, chapter):
        html = await self.fetch_page(chapter.get('path'))
        html = await self.loop.run_in_executor(self.executor, BeautifulSoup, html, 'html5lib')
        script = self.select_one(html, '#main > script')
        if script != '':
            script = script.replace('document["mj"]', 'obj')
            script = js2py.eval_js(script)
            for img in script.im:
                chapter.get('imgs').append(img.get('u'))

    async def get_latest_mangas(self):
        mangas = []
        mangas_to_process = []
        html = await self.fetch_page('latest')
        html = await self.loop.run_in_executor(self.executor, BeautifulSoup, html, 'html5lib')
        manga_links = html.select('.d46 a')
        if manga_links is not None:
            manga_links = manga_links[:self.to_fetch]
            for link in manga_links:
                mangas.append(link.get('href').replace('/', ''))
        for manga in mangas:
            mangas_to_process.append(self.loop.create_task(self.get_manga_data(manga)))
        await asyncio.gather(*mangas_to_process)
            

    async def fetch_page(self, path):
        async with sema, ClientSession() as session:
            url = f'{self.base_url}/{path}'
            while True:
                try:
                    async with session.get(url) as resp:
                        resp = await resp.read()
                        html = resp.decode('utf-8')
                        return html
                except ClientConnectionError:
                    print('ooops, connection error')

    def slugify(self, title, to_lower=True):
        return slugify(title, to_lower=to_lower)

    def select_one(self, html, selector):
        txt = html.select_one(selector)
        if txt is not None:
            return txt.get_text(strip=True)
        return ''

    def select_capa(self, html, selector):
        txt = html.select_one(selector)
        if txt is not None:
            return txt.get('src')
        return ''

    def select_genres(self, html, selector):
        genres = []
        tags = html.select(selector)
        if tags is not None:
                for tag in tags:
                    if tag.get_text(strip=True) != '':
                        genres.append(tag.get_text(strip=True))
        return genres

    def select_chapters(self, html, selector):
        chapters = []
        tags = html.select(selector)
        if tags is not None:
                for tag in tags:
                    chapters.append({
                        'name': tag.parent.get_text().split(':')[-1].strip(),
                        'number': tag.get('href').split('/')[-1],
                        'path': tag.get("href"),
                        'imgs': []
                    })
        return chapters

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    sema = asyncio.Semaphore(100)
    scrapy = Scrapy(loop, sema, to_fetch=100)
    start = time()
    loop.run_until_complete(scrapy.start())
    print('Time Elapsed:', strftime('%H:%M:%S', gmtime(time() - start)))