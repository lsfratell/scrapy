from pony.orm import *
from datetime import datetime

db = Database('postgres', user='postgres', password='123456', database='mangashine')

class Manga(db.Entity):
    _table_ = 'mangas'

    id = PrimaryKey(int, auto=True)
    name = Required(str)
    slug = Required(str, unique=True)
    year = Optional(str)
    status = Optional(str)
    direction = Optional(str)
    description = Optional(str)
    capa = Optional(str)
    views = Required(int, default=0)
    chapters = Set('Chapter')
    imgs = Set('Img')
    ch_updated_at = Required(datetime, default=datetime.utcnow())
    genres = Set('Genre', table='manga_genres', column='genre_id')
    created_at = Required(datetime, default=datetime.utcnow())
    updated_at = Required(datetime, default=datetime.utcnow())

class Chapter(db.Entity):
    _table_ = 'chapters'

    id = PrimaryKey(int, auto=True)
    number = Required(int)
    name = Optional(str)
    views = Required(int, default=0)
    manga = Required('Manga', column='manga_id')
    imgs = Set('Img')
    created_at = Required(datetime, default=datetime.utcnow())
    updated_at = Required(datetime, default=datetime.utcnow())

class Img(db.Entity):
    _table_ = 'imgs'

    id = PrimaryKey(int, auto=True)
    path = Required(str)
    chapter = Required('Chapter', column='chapter_id')
    manga = Required('Manga', column='manga_id')
    created_at = Required(datetime, default=datetime.utcnow())
    updated_at = Required(datetime, default=datetime.utcnow())

class Genre(db.Entity):
    _table_ = 'genres'

    id = PrimaryKey(int, auto=True)
    name = Required(str)
    slug = Required(str, unique=True)
    views = Required(int, default=0)
    mangas = Set('Manga', table='manga_genres', column='manga_id')
    created_at = Required(datetime, default=datetime.utcnow())
    updated_at = Required(datetime, default=datetime.utcnow())

db.generate_mapping()